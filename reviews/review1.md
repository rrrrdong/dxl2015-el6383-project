
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
The experimenter looks to analyze the bandwidth incompatibility between TCP vegas and TCP reno. TCP vegas which is a more recent one tends to adjust it's badnwidth appropriately depending on the expected throughput
to achieve a higher throughput than the traditional TCP reno.


2) Does the project generally follow the guidelines and parameters we have
learned in class?
Yes, The project is well organized and follows all the guidelines and paramters as we have learned. It has a specific set of metric which is being measuured i.e Bandwidth. Also it varies Buffer size of both TCP vegas and Reno
which is same at a certain instant of time which is needed to get accurate results for comparison.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of this experiment is to analyze what happens when TCP vegas and TCP reno are being used. The amount of bandwidth both of these consume and the throughput that they give.
After analysis we will be able to say which TCP variant is better based on consumption of bandwidth. The goal is very specific as there is a comparison on two variants only based on one metric.
This will lead to solid results and conclusion.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
The metric's and parameters are chosen right for this goal as it wishes to calculate the bandwidth consumption, It rightly measures the bandwidth consumed by using iperf.
It varies the buffer size which is correct as the buffer size is changed the bandwidth consumed by these variants is also reflected which will then lead us to concrete results for various situations.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
I feel the experimenter could have more trials. The experimenter varies the buffer size with 4 trials. Maybe a couple more would be better for more concrete results.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
The metrics selected to study are absolutely right. The metric leads us correct conclusion successfully. As the bandwidth consumption is compared. The experimenter could not use anything else other
than iperf to meaure the bandwidth.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
Parameters of the experiment are not meaningful. The range is a little abrupt and does not make sense as the minimum buffer size goes from 4 40 409 4096. The difference is not at all accurate and
does not make sense. As the inital 2 paramters are under 50 and third is near about 400 times more and the third one is 3500 times more. So it is not very meaningful.
The experimenter could have improved on these ranges to be meaningful and which made sense.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
They have not addressed the possibility of interactions as such. I feel there could be one if the parameters were changed again and the entire experiment was performed again.
As the parameters do not make sense the results obtained may not be as concrete as they could be.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
Yes the baseline selected is appropriate and could be very well implemented in a realistic environment with a few minor changes. The results that will be obtained in a realistic environment
will be similar to the results obtained in the geni testbed.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes, They have reported the quantitative results of the experiment and then used it to plot the graph from where the conclusions were drawn.

2) Is there information given about the variation and/or distribution of
experimental results?
I did not encounter any information regarding the variation or distribution of the experimental results.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
Yes, There is data integrity throughout the experiment. At no point I felt like the data was being manipulated.
Except for once in the Rscript as we took results for 4 different parameters but while plotting we only used 3 parameters for comparison.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
The data is clear, However I feel instead of a violin plot or in addition to violin plot the experimenter could have made use of the box plot.
Box plot could have provided the result in a more accurate form and easy to read and interpret than the violin plot.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
Yes, the conclusions drawn by the experimenter are supported by the results seen and the plots that have been plotted during the experiment.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
Yes, the instructions were clear and easy to understand and follow accordingly.

2) Were you able to successfully produce experiment results?
Yes, I was able to reproduce the experiment results as produced by the experimenter. The instructions to go about the experiment were clear and precise.

3) How long did it take you to run this experiment, from start to finish?
The experiment took about 2 hours to perform. Additionally, while reserving resources it took more than 30-40 mins to successfully reserve all resources and to get ready.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
I had to go back to lab2b for reference to create a slice and reserve resources for rstudio. In the rtudio script I had to make changes according to my filenames as the filenames were different. Also I had to clone my repository
and push all results in my repository which was not mentioned in the instructions. After this I had to clone again in rstudio to get the results so that I could then plot the results using R script, This was also not mentioned
in the document I had to refer to lab2b for this entire process.
Apart from this the Rspec file was not uploaded. So I had to contact the experimenter to get the Rspec file.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
I think with all the setting up reources and running six nodes at a time. This would fall in category 3. As few considerable efforts were required to successfully carry out this experiment.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

The experimenter could have also used boxplot or used boxplot instead of violin which I felt is a better option to deliver the results.
The parameters that were being varied that is buffer size could have been better. Their range were inaccurate and did not make sense on how it was chosen. The results would have been better
if the range and difference between the parameters was more accurate and which made more sense.
Also while plotting the results using Rstudio taking all four results for the plot seemed the right way to do it. As I could see the script load only 3 trails of data.
