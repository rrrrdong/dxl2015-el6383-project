
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

The experiment analyzes the incompatibility of TCP Vegas and TCP Reno.
In detail, the experiment is intended to verify the unfairness bandwidth
sharing when a TCP Vegas competes with TCP Reno. This experiment aims at
observing and comparing the throughput performance of TCP variants with
different buffer sizes.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

The project has the following systematic approach defined - Goals,Services,
metrics,parameters,softwares used and interactions between parameters.

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal of the experiment is to analyze the incompatibility of TCP Vegas
and TCP Reno. In detail, the experiment is intended to verify the unfairness
in bandwidth sharing when TCP Vegas competes with TCP Reno.This experiment
is conducted by observing and comparing the throughput performance of TCP
variants with different buffer sizes. The goal is specific and focused as
it mentions clearly how and what is the intention of the experiment.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

Throughput of two connections with TCP Vegas and TCP Reno is the metrics
chosen for the experiment and the parameters are TCP variants and buffer size.
Since the experiment intends to verify the unfairness in bandwidth sharing,
measurement of throughput sufficies the goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

There were 3 trials conducted for each experimental unit of TCP Reno and TCP Vegas.
In my opinion these were sufficient amount of trials required for the graphic
comparison of bandwidth utilisation by the TCP variants.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

The aim of the experiment was to show how TCP Reno eats up the bandwidth
when competing with TCP Vegas.This can be best depicted by calculating
throughput as it is a direct measure of bandwidth utilisation.Hence the metric
selected is clear and unambiguous.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

The parameters are TCP variants and buffer size which are meaningful with respect
to the experiment design. There are 4 values of buffer size provided but the author
does not exactly talk about why were these values preferred or how were these values selected.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

Yes, the authors have sufficiently address the interactions between parameters.
The two parameters considered in the experiment were TCP variants and buffer size.
It was shown that as the buffer size was increased,the unfairness of bandwidth
utilisation between the TCP variants increase.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

The comparisons are made reasonably as throughput is a proper metric to depict
the bandwidth utilization by TCP variants.

## Communicating results

1) Do the authors report the quantitative results of their experiment?

The quantitative results are reported using 4 plots which show the comparison of TCP Vegas and TCP Reno in
the 4 different  scenarios considered in the experiment.

2) Is there information given about the variation and/or distribution of
experimental results?

The parameters varied are TCP variants and buffer size. The authors have
clearly mentioned how these parameters are to be varied during the course
of the experiment in order to obtain meaningful results.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

While the experiment was being conducted, the results were obtained in clear manner
and there was no ratio games played to make the results look better.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

There were 4 graphs plotted for the four cases described in the experiment. Each
graph represented 3 trials each of TCP Reno and TCP Vegas. The results could be
compared easily for the 4 cases. The graphs plotted were line plots and violin plots
depicting clearly the difference in the throughput of TCP Reno and TCP Vegas for four
cases of buffer length.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

Yes, the experiment result depicts the unfairness in bandwidth utilisation
as stated in the conclusions. The results can be verified by the values of bandwidth
collected on the two servers for TCP Reno and TCP Vegas.


## Reproducible research

1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The instructions were organised in three categories - Experiment Setup,
Experiment execution and using raw data for
graphically representing the results.

2) Were you able to successfully produce experiment results?

Yes the results were successfully represented for the scenarios described
in the experiment. Raw data was collected for all the four cases and plotted respectively.

3) How long did it take you to run this experiment, from start to finish?

It took 30 minutes to 1 hour to run this experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

No I followed the steps mentioned in the documentation and arrived at the results. The entire experiment setup along with graphical representation was provided.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

Degree 5

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

Packet drop policy can provide some fairness for TCP Reno and TCP Vegas in the above experiment design.
With packet drop policy, it can server as the parameter and extend this experiment showing
how fairness can be achieved for bandwidth sharing when TCP Reno and TCP Vegas are deployed.
