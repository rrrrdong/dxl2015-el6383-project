
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
The experiment is set to determine the unfairness in bandwidth when two TCP variants, TCP vegas and reno, are compared to one another in the same topology. The topology is provided and has six nodes. Of these nodes, Node 0 (N0) runs on TCP vegas while N1, N2, N3, N4, N5 run on TCP reno. The experimenter has set N4 and N5 as servers  and N0 and N1 as the clients. The TCP Bufer size is a parameter that is changed and different sets of outputs are obtained for TCP vegas and reno. The results are .txt files that consists the Throughput of a TCP variant. The results are communicated effectively using R plots.

2) Does the project generally follow the guidelines and parameters we have
learned in class?
The project follows most of the guidelines and parameters learnt in the class. The goal of the experiment is clear and specific enough to understand what the experimenter is trying to achieve in the experiment. The experimenter has measured a metric and has provided certain outputs by varying the parameter. This helps us get the overall idea of what the experimenter is trying to convey using the experiment. The results obtained are also presented in the form of plots which effectively compare the two TCP variants and allows us to reach a proper conclusion.
## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of the experiment is to design a project and analyze the incompatibility of TCP Vegas and TCP Reno in a topology. It can be used to verify the unfairness of bandwidth sharing that occurs when TCP vegas and reno competes each other in the same topology. This goal seems focussed and specific as the experimenter has mentioned the variants that we would be using while performing the experiment. The goal also conveyed what we intend to achieve at the end of experiment and how is it useful in some other applications. The experiment is likely to produce certain results that would compare the performance of two TCP variants in the topology.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
The parameters chosen to be varied in the experiment is TCP Buffer size. There is a minimum, default and maximum value assigned that keeps on changing for every trial. The values are increased almost 10x for each trial and corresponding results are observed. This presents quite some results which prove enough to achieve the goal of experiment.
On the other hand, the metric chosen is Throughput. The experimenter measures throughput and accordingly compares the two variants of TCP. Based on the value of this metric, the experimenter has stated his conclusion which mentions the variant that is better among the two

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
The experiment is well designed and obtains enough results to reach a decisive conclusion. Although, we do need to perform more trials to be conclusive of the results. We have considered four trials that present the outputs for four cases. So, it does take some number of trials to make sure that we have achieved our goal of experiment.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
The metric studied is Throughput which is calculated for each and every ping from client to server. The Throughput is a right metric as it effectively creates a comparison between the two variants and gives an unambiguous result. There is no confusion created when Throughput values are observed over different ping messages sent thus helping to achieve the goal of experiment.
5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
The parameters used for the experiment is TCP Buffer Size. There is a minimum, default and maximum size of buffer mentioned. These values are varied by multiplying the previous value by at around 10x. This gives us results over a wide range of buffer size and conclusive to achive the goal of experiment.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
There is only one parameter that varies to give a conclusive result for the experiment performed and achieving the goal. The authors have not considered ny other parameters and so there is no comparison between parameters made in the experiment performed.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
The comparison is made between the two TCP variants (vegas and reno). Apart from that, no other comparisons are made since the experimenter has varied only a single parameter.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
The authors have efficiently reported the quantitaive results of the experiments. The results sem conclusive enough to achieve the goal of this experiment.

2) Is there information given about the variation and/or distribution of
experimental results?
The information is provided about the variation and distibution of the experimental results. There are various graphs provided for the outputs obtained as a result of varying the TCP buffer size.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
The data represented seem to be accurate with the data obtained while reviewing the experiment. So, there appears to be no ambiguity with the data represented in the experiment produced results.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
The data is presented or communicated using the R plot. It gave a pretty clear cut comparison between the two variantsand how their results appear graphically while comparison. The grahical form is used to communicate the results effectively and it is appropriate with the goal forich the data is intended for.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
The conclusion drawn with the experiment graph is that TCP reno takes up majority of the available bandwidth and there is not much bandwidth available for TCP vegas. This difference or unfairness in the bandwidth gap deepens as the TCP buffer size is increased. This was presented in the graphical form. This pretty much achieves the author's goal of comparing the two TCP variants and figuring out the unfairness that exists when TCP vegas and reno are simultaneously used in a network topology.


## Reproducible research

1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
The instructions were clear and it was not very difficult to understand what the author tries to explain the steps and carry out the experiment. The instructions were properly arranged to make it easy for the experiment to be reproduced or reviewed. Although, there was a major problem in the beginning. That problem being that though the RSpec file was mentioned which was necessary to create the topology, but it wasn't present anywhere in the files provided. The author did provide with the file when requested, but without the file not present, it would've been a lot more difficult to reproduce the results that author wanted to convey in the experiment. There was no way apart from the RSpec file, that the experiment could have been successfully performed with only the resources mentioned by the author.

2) Were you able to successfully produce experiment results?
The results were successfully reproduced but that was only after the RSpec file was provided. Without the file, it was very difficult to reproduce the result of the experiment.

3) How long did it take you to run this experiment, from start to finish?
The experiment took around 90 minutes to be able to perform completely. This was because some time was required to figure out the RSpec file and if it is present in the files provided. Once clear of its absence, it was requested to the author. The author was prompt and did provide the file for experiment review.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
I did not make any changes or did not add any additional steps beyond the documentation for successfully prducing the experiment results. There was just one bit of a confusion in one of the steps in between but that had nothing to do with the alacrity of the steps mentioned.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
In the lecture, there were six degrees of reproducibility. The highest rating was 5 and minimum was 0. I would rate this experiment as 3. This is because the RSpec file was not provided and that took some efforts to procure. Apart from that, there were a few minor steps not mentioned and so reviewer had to go through some other resources to figure out. Also, time was an important factor. Since, the time required and efforts required are quite considerable, I would rate this as 3.


## Other comments to authors
Just make sure that all the files are properly attached and it would be better if you had attached an image with some important steps, so that reviewer can figure out easily how to proceed to further steps.
Please write any other comments that you think might help the authors
of this project improve their experiment.
